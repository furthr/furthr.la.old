'use strict';

var furthr = angular.module('furthrlaApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute'

])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'HomeController'
      })
      .otherwise({
        redirectTo: '/'
      });
  });



furthr.controller('HomeController', function($scope, $window) {
    //alert("Home");

});

furthr.controller('SettingsController', function($scope) {

});

furthr.controller('SoftwareController', function($scope) {
    
 
});

furthr.controller('NavigationController', function($scope) {

});

furthr.controller('ProductionController', function($scope) {

});

furthr.controller('MarketingController', function($scope) {

});

furthr.controller('FurthrController', function($scope) {

});

furthr.controller('ContactController', ['$scope', 'userService', function($scope) {
    
    //$scope.user = {
    //   email: "me@example.com"
    //};

    $scope.signupForm = function() {
        // POST email service
        $('#myModal').modal();
        //$scope.showingContactForm = true;

        // Show further information

    };

    $scope.facebookLink = function() {
        $('#facebookLink').modal();
    };

}]);

furthr.service('userService', ['$http', function($http) {
  var registerUser = function() {

  }
}]);

furthr.directive('ensureUnique', function($http) {
    return {
        require: 'ngModel',
        link: function(scope, ele, attrs, c) {
            scope.$watch(attrs.ngModel, function() {
                $http({
                    method: 'POST',
                    url: '/api/check/' + attrs.ensureUnique,
                    data: {'field': attrs.ensureUnique}
                }).success(function(data,status,headers,cfg) {
                    c.$setValidity('unique', data.isUnique);
                }).error(function(data,status,headers,cfg) {
                    c.$setValidity('unique', false);
                });
            });
        }
    };
});


furthr.directive('serviceDirective', function() {
    return {
        restrict: 'A',
        replace: false,
        scope: {
            serviceIdentifier: '@'
        },
        templateUrl: 'templates/serviceTemplate.html',
        controller: function($scope) {            
            
            $scope.togglePreview = function () {
              if (!$scope.showingPreview) $scope.showingPreview = true;
              else $scope.showingPreview = false;
              
            };

            $scope.showPreview = function () {              

              $scope.thumbnails = [
                {
                    thumbnail: "assets/games/godd.png",
                    title: "testing"
                },
                {
                    thumbnail: "assets/games/terrorats.png",
                    title: "testing"
                },
                {
                    thumbnail: "assets/games/zlider.png",
                    title: "testing"
                }
              ];

              if (!$scope.showing) {
                $scope.showing = true;
                
              } else {
                $scope.showing = false;
                
              }

            }          
            
        }
    };
});

furthr.directive('contactDirective', function() {
    return {
        restrict: 'A',
        replace: false,
        scope: {
            serviceIdentifier: '@'
        },
        templateUrl: 'templates/contactTemplate.html'
    };
});

furthr.directive('furthrDirective', function() {
    return {
        restrict: 'A',
        replace: false,
        scope: {
            serviceIdentifier: '@'
        },
        templateUrl: 'templates/furthrTemplate.html'
    };
});




